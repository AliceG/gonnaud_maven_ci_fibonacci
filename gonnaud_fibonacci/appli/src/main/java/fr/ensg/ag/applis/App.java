package fr.ensg.ag.applis;


import fr.ensg.ag.fibonacci.core.Sequence;
import org.springframework.web.bind.annotation.PathVariable;

@org.springframework.boot.autoconfigure.SpringBootApplication
public class App {
    public static void main( String[] args ) {
        org.springframework.boot.SpringApplication.run(App.class, args);
    }

    @org.springframework.web.bind.annotation.RestController
    public static class Fibonacci {

        @org.springframework.web.bind.annotation.GetMapping("/fibonacci/{n}")
        public String fibonacci(@PathVariable("n") String int_n) {

            int n = Integer.parseInt(int_n);
            return String.valueOf(Sequence.compute_fibonacci(n));
        }
    }
}
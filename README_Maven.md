# Résumé

Retours sur le projet partie Maven.

Voir le *README* pour la présentation générale.


# Modules implémentés

Les 3 modules attendus sont implémentés : 

* Un module core qui calcule la suite de Fibonacci, et les tests unitaires associés
* Une application Spring-Boot. Après "mvn verify", l'exécutable (jar) est créé (répertoire target) et peut être exécuté. On peut alors utiliser l'application à l'adresse suivante : _http://localhost:8080/fibonacci/13_ , où 13 (un exemple) est l'entier à passer en argument du calcul de la suite de Fibonacci. Le résultat s'affiche dans la page.
* Un plugin Maven. Après "mvn verify", on peut appeler le plugin de la manière usuelle, avec la ligne suivante : _mvn fr.ensg.ag.maven.plugins:fibonacci-maven-plugin:1.1-SNAPSHOT:fibonacci -Drank=13_ , où 13 (un exemple) est l'entier à passer en argument du calcul de la suite de Fibonacci. Le résultat s'affiche dans le terminal.
* De plus, la documentation est générée automatiquement ("mvn site") (répertoire target/site).

La commande "mvn verify site" (testée avant livraison) génère ainsi la documentation, exécute les tests, construit les binaires, et rend l'application et le plugin utilisables. 


# Difficultés rencontrées

Les parties abordées en TP ont été relativement aisées à adapter à ce sujet. 

Les commits sont une trace du temps passé. De la création du projet à l'obtention d'un squelette, avec plugin Maven et sans application Spring-Boot, il m'a fallu environ 3h. Les parties qui m'étaient inconnues parce que je n'avais pas eu le temps de les traiter en TP, Spring-Boot et la documentation, ont été beaucoup plus chronophages (environ 2h à elles seules). Il y a en effet beaucoup de perte de temps à trouver quelle balise mettre où, avec quelle url, etc. Le TP n'étant pratiquement pas guidé, il a fallu chercher beaucoup d'indications sur internet.

Je n'avais pas eu l'occasion de travailler sur la publication en TP non plus. J'ai cherché à produire une release avec le plugin maven-release-plugin, en ajoutant le "distributionManagement" et le SCM. Le release:prepare a fini par s'exécuter, mais pas le release:perform (dépendances non trouvées). J'ai donc fini par abandonner au vu du temps déjà passé sur le projet (après 1h de tests à tâtons). Le versionnage du dépôt laisse quelques traces de ces recherches. Là encore, les informations minimalistes du support de cours ("ajouter ceci" sans donner la syntaxte et les éléments nécessaires) ne m'ont pas assez aidée. Les informations sur internet sont dures à trouver pour un contexte en particulier, ce qui rend les soucis de dépendances (toujours très spécifiques au projet...) très durs à élucider juste avec l'appui des messages d'erreur et d'internet.

Rétrospectivement, malgré le fait qu'une partie des notions n'avait pas été faite en TP par manque de temps, j'ai réussi à fournir les modules demandés (i.e. tout sauf release) dans le temps préconisé.
# Résumé

Retours sur le projet partie Intégration Continue.

Voir le *README* pour la présentation générale.

# Solutions choisies 

Le code est déposé sur GitLab, dans un dépôt public. On utilise pour l'intégration continue la solution GitLab proposée.

# Pipeline

La pipeline est dans le fichier _.gitlab-ci.yml_ à la racine du projet (répertoire _gonnaud_fibonacci_). Elle a été générée à partir du template Maven proposé par GitLab, puis lourdement remaniée pour correspondre aux exigences attendues et au contexte du projet. Sa syntaxe et les quelques commentaires la rendent explicite.

Les 3 étapes attendues sont fonctionnelles : 

* Build, qui package le projet (génère l'application jusqu'aux tests d'intégration exclus) et crée la documentation automatique de chaque module (sites) (Java 8). La documentation générée est archivée pour être déployée en dernière étape.

* Test, qui lance les tests d'intégration (non implémentés ici) en parallèle avec Java 8 et Java 11.

* Deploy, qui récupère les fichiers du site généré étape 1 et le déploie sur la page Public de GitLab. Cette page est accessible via _Settings > Pages_, ou directement à l'adresse suivante : https://aliceg.gitlab.io/gonnaud_maven_ci_fibonacci/

La pipeline est fonctionnelle (succès des 3 étapes pour le code livré).

# Merge Requests

Les merge requests ne seront acceptées que si la pipeline n'échoue pas. Pour cela, j'ai activé l'option _Settings > Merge request > Only allow merge requests to be merged if the pipeline succeeds_. 

J'ai ensuite testé (avec succès) ce comportement. Les traces de ces tests sont visibles dans l'historique des commits, branchs et CI Pipelines ; et surtout l'historique des merge requests. Pour cela, je me suis connectée depuis un compte tiers (Alice Test). J'ai cloné le dépôt, créé une branche test, et testé deux commits : un commit cassant les TU, et un validant la pipeline. Après chacun des commits (push sur la branche test), j'ai fait une demande de merge request. Comme attendu, le merge cassant a été refusé ; l'autre accepté.

# Difficultés rencontrées

Le projet s'est avéré très chronophage (beaucoup de temps passé pour peu de lignes à écrire !). Les commits donnent une trace du temps passé (dernier commit pour la partie Intégration Continue, hors README : "Update .gitlab-ci.yml PIPELINE FONCTIONNELLE (3 stages)"). Notons que certains commits ont été effectués aux pauses sans qu'il n'y ait de travail en dehors, ce qui signifie que deux commits espacés de 2h ne traduisent pas 2h de travail. Globalement, je pense avoir travaillé autour de 5h sur cette partie (hors README). 

Tout le challenge a été de trouver les bonnes syntaxes, en particulier sur les dépendances, l'ordre des étapes, et les chemins. En l'absence de documentation en ligne adaptée à notre cas spécifique, il s'agit d'un long processus de tâtonnement - en témoigne l'historique des pipelines. C'était en effet un outil entièrement nouveau, pas du tout présenté en cours. Les problèmes de syntaxe ou de dépendances et versions sont de ce fait très difficiles à identifier (même si la solution tient en quelques caractères !). Les pistes sur internet ne sauraient donner des solutions s'appliquant précisément à notre projet Maven avec ses modules et ses dépendances ; ni à la pipeline demandée en particulier. Les échecs de la pipeline ont mené à rajouter de nouvelles indications dans les POM pour permettre l'automatisation d'étapes, et surtout à remanier le YML.
